function showNotification(title, body, icon, data) {
  var notificationOptions = {
    body: body,
    icon: icon,
    tag: 'localhost-sample-notification',
    data: data,
    sticky: true,
    actions: [
      { action: "CONFIRMAR", title: "Confirmar!" },
      { action: "CANCELAR", title: "Cancelar" }
    ],
    requireInteraction: true
  };

  self.registration.showNotification(title, notificationOptions);
  return;
}

self.addEventListener('activate', function(e) {
  const ws = new WebSocket('wss://040e61ad.ngrok.io/?accessKey=c81833da5cb24271282eac0a3cc7a6fb&client=35673&groups=37063&auth={%22token%22:%22UVIl4kgbrtginUE1mEtnkQtT6x8gMomHEtPFsALVS1LzYK7vTi0ItYyfUXZSByIb%22}');
  ws.addEventListener('open', (...args) => console.log(...args));
  ws.addEventListener('message', (data) => {
    const dataParsed = JSON.parse(data.data);
    showNotification(dataParsed.data.title, dataParsed.data.body, dataParsed.data.img, {
      id: dataParsed.id,
      event: dataParsed.event
    });
  });
  ws.addEventListener('error', (...args) => console.log(...args));
});

self.addEventListener('notificationclick', function(e) {
  console.log(`[${e.action}]`, e.notification.data);
  e.srcElement.clients.openWindow('/#/' + e.action);
  e.notification.close();
});
